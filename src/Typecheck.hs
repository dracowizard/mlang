{-# LANGUAGE LambdaCase, FlexibleContexts, OverloadedStrings, DeriveDataTypeable #-}
{-# LANGUAGE TupleSections, DuplicateRecordFields, NamedFieldPuns, TypeApplications #-}

module Typecheck where

import Data.Function
import Data.Foldable
import Data.Maybe
import Data.Traversable
import Data.Int (Int64)

import Data.Text (Text)

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M

import Control.Monad.Writer
import Control.Monad.State

import Data.Generics

import Ast

loadAstSymbols :: MonadWriter Text m => AstSymbols -> [AstTop] -> m AstSymbols
loadAstSymbols = foldM addSym
    where addSym astSyms@(AstSymbols os fs vs ts) = \case
        AstTopFixity o@(AstFixity _ _ n _) -> if M.notMember n os
            then return $ astSyms { astOperators = M.insert n o os }
            else astSyms <$ tell ("operator " <> n <> " multiple declarations\n")
        AstTopFn f@(AstFn n _ _ _) -> if M.notMember n fs
            then return $ astSyms { astFunctions = M.insert n f fs }
            else astSyms <$ tell ("function " <> n <> " multiple declarations\n")
        AstTopVar v@(AstVarDecl n _ _) -> if M.notMember n vs
            then return $ astSyms { astVariables = M.insert n v vs }
            else astSyms <$ tell ("variable " <> n <> " multiple declarations\n")
        AstTopType (AstTypeDecl n t) -> if M.notMember n ts
            then return $ astSyms { astTypes = M.insert n t ts }
            else astSyms <$ tell ("type " <> n <> " multiple declarations\n")

data SymOp = SymOp
    { symOpAssoc :: Assoc
    , symOpPrec :: Int64
    , symOpLeftType :: SymType
    , symOpRightType :: SymType
    , symOpResultType :: SymType
    , symOpBody :: [AstStmt]
    } deriving (Show, Data)

data SymFn = SymFn
    { symFnArgs :: Map Text SymType
    , symFnReturns :: Maybe SymType
    , symFnBody :: [AstStmt]
    } deriving (Show, Data)

data SymVar = SymVar
    { symVarType :: SymType
    , symVarValue :: AstExpr
    } deriving (Show, Data)

data SymStruct = SymStruct
    { symStructBody :: Map Text SymType
    } deriving (Show, Data)

data SymEnum = SymEnum
    { symEnumBody :: Map Text SymType
    } deriving (Show, Data)

data SymType
    = SymTypeStruct SymStruct
    | SymTypeEnum SymEnum
    | SymTypeInt
    | SymTypeFloat
    | SymTypeString
    | SymTypeBuilding
    | SymTypeUnit
    | SymTypeBlock
     deriving (Show, Data)

builtins :: Map Text SymType
builtins = M.fromAscList
    [ ("block", SymTypeBlock)
    , ("building", SymTypeBuilding)
    , ("float", SymTypeFloat)
    , ("int", SymTypeInt)
    , ("string", SymTypeString)
    , ("unit", SymTypeUnit)
    ]

data GlobalSymbols = GSymbols
    { gsymOperators :: Map Text SymOp
    , gsymFunctions :: Map Text SymFn
    , gsymVariables :: Map Text SymVar
    , gsymTypes :: Map Text SymType
    } deriving (Show, Data)

convTypes :: MonadWriter Text m => Map Text AstType -> m (Map Text SymType)
convTypes ast = M.mapMaybe id $ foldM mempty conv $ M.toList ast
    where 
        conv :: MonadWriter Text m => Map Text (Maybe SymType) 
            -> (Text, AstType) -> m (Map Text (Maybe SymType))
        conv acc elem@(name, ty) = curry M.union <$> convType name ast (acc, mempty) elem

convType :: MonadWriter Text m 
    => Text -> Map Text AstType -> (Map Text (Maybe SymType), Map Text (Maybe SymType))
    -> (Text, AstType) -> m (Map Text (Maybe SymType), Map Text (Maybe SymType))
convType errName ast (globals, locals) (name, ty) = case ty of
        AstTypeStruct (AstStruct body) -> convBody (SymTypeStruct . SymStruct) body
        AstTypeEnum (AstEnum body) -> convBody (SymTypeEnum . SymEnum) body
        AstTypeNamed (AstNamedType nameRef) -> case M.lookup nameRef builtins of
            Just sym -> return (globals, M.insert name (Just sym) locals)
            Nothing -> do
                let refExists = M.member nameRef ast
                if refExists then return () else tell $
                    "type " <> errName <> " uses nonexistant type " <> nameRef
                newGlobals <- case M.lookup nameRef globals of
                    Just _ -> return globals
                    Nothing -> fromMaybe (return globals) $ M.lookup nameRef ast
                        <&> curry M.union . convType nameRef ast (globals, mempty)
                return (newGlobals, M.insert name (M.lookup nameRef newGlobals) locals)
    where
        convBody constr body = do
            (newGlobals, newBody) <- foldM (globals, mempty) (convType ast) body
            let hasNothing = not $ null $ listify isNothing newBody
            return $ if hasNothing
                then (newGlobals, M.insert name Nothing locals)
                else (newGlobals, M.insert name (Just $ constr $ M.mapMaybe id newBody) locals)

toGlobalSymbols :: MonadWriter Text m => AstSymbols -> m GlobalSymbols
toGlobalSymbols ast = do
        types <- convTypes $ astTypes ast
        vars <- convVars types $ astVariables ast
        fns <- convFns types $ astFunctions ast
        ops <- convOps types fns $ astOperators ast
        return $ GSymbols ops fns vars types
    where
        convVars types = foldM mempty $ \acc (AstVarDecl name type' value) ->
            case M.lookup type' types of
                Just type'' -> return $ M.insert name (SymVar type'' value) acc
                Nothing -> tell ("variable " <> name <> " uses nonexistant type " <> type')
                    >> return acc
