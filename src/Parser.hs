{-# LANGUAGE OverloadedStrings, FlexibleContexts, ConstraintKinds, DeriveDataTypeable #-}

module Parser where

import Data.Void
import Data.Functor
import Data.Maybe
import Data.Int (Int64)
import Data.Char
import Data.Bifunctor
import Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Map.Strict as M
import Data.Map.Strict (Map)
import qualified Data.Sequence as S
import Data.Sequence (Seq(Empty, (:<|), (:|>)))
import Data.Typeable
import Data.Traversable

import Control.Monad.Writer.Strict

import Text.Megaparsec
import Text.Megaparsec.Char hiding (space)
import qualified Text.Megaparsec.Char.Lexer as L

import qualified Prettyprinter as P
import Prettyprinter (Pretty, Doc, pretty)

import Data.Generics hiding (empty)

import Ast

type MonadParser m = MonadParsec Void Text m

parseFile :: String -> IO (Either Text [AstTop])
parseFile file = do
    parse' <- first (T.pack . errorBundlePretty)
        <$> (runParser pTopLevel file <$> T.readFile file)  
    return $ case parse' of
        Left err -> Left err
        Right ast -> let (res, err) = runWriter $ fixOperatorPrecedence ast
            in if T.compareLength err 0 == EQ then Right res else Left err

space :: MonadParser m => m ()
space = L.space hspace1 (L.skipLineComment "//") (L.skipBlockComment "/*" "*/")

lexeme :: MonadParser m => m a -> m a
lexeme = L.lexeme space

symbol :: MonadParser m => Text -> m Text
symbol = L.symbol space

parens, braces, brackets :: MonadParser m => m a -> m a
parens = between (symbol "(") (symbol ")")
braces = between (symbol "{") (symbol "}")
brackets = between (symbol "[") (symbol "]")

newlines :: MonadParser m => m ()
newlines = () <$ many (lexeme eol)

nlsep :: MonadParser m => Text -> m ()
nlsep sep = try (() <$ symbol sep) <|> newlines

identifier, operator :: MonadParser m => m Text
identifier = lexeme $ label "identifier" $
    T.cons <$> letterChar <*> takeWhileP Nothing isAlphaNum
operator = lexeme $ label "operator" $
    takeWhile1P Nothing (`elem` ("~`!@#$%^&*-=+<>,./?" :: String))

int :: MonadParser m => m Int64
int = lexeme $ label "int" $ L.signed empty L.decimal

float :: MonadParser m => m Double
float = lexeme $ label "float" $  L.signed empty L.float

pTopLevel :: MonadParser m => m [AstTop]
pTopLevel = optional space *> newlines *> some (choice
    [ AstTopFn <$> pFn
    , AstTopType <$> pTypeDecl
    , AstTopVar <$> pVarDecl
    , AstTopFixity <$> pFixity
    ] <* newlines)

fixOperatorPrecedence :: MonadWriter Text m => [AstTop] -> m [AstTop]
fixOperatorPrecedence ast = forM ast $ everywhereM (mkM fixOp)
    where
        ops = M.fromList $ mapMaybe transOp ast
        transOp (AstTopFixity f) = Just (astFixityOp f, f)
        transOp _ = Nothing

        getOp :: MonadWriter Text m => Text -> m AstFixity
        getOp op = case M.lookup op ops of
            -- The dummy return value is chosen so the ast will not be changed.
            Nothing -> AstFixity AssocRight 0 "" "" <$ tell ("invalid operator " <> op <> "\n")
            Just fixity -> return fixity
        fixOp op@(AstOp exprlhs oplhs rhs) = case rhs of
            AstExprOp (AstOp exprmid oprhs exprrhs) -> do
                AstFixity assoclhs preclhs _ _ <- getOp oplhs
                AstFixity assocrhs precrhs _ _ <- getOp oprhs
                case (preclhs < precrhs, preclhs > precrhs, assoclhs, assocrhs) of
                    (False, False, AssocLeft, AssocRight) -> fmap (const op) $ tell $
                        "operator " <> oplhs <> " is left associative, operator " <> oprhs <>
                        " is right associative at the same precedence; use parentheses\n"
                    (False, False, AssocRight, AssocLeft) -> fmap (const op) $ tell $
                        "operator " <> oplhs <> " is right associative, operator " <> oprhs <>
                        " is left associative at the same precedence; use parentheses\n"
                    (False, False, AssocRight, _) -> return op
                    (True, _, _, _) -> return op
                    (_, _, _, _) -> return $ AstOp (AstExprOp $ AstOp exprlhs oplhs exprmid) oprhs exprrhs
            _ -> return op

pBlock :: MonadParser m => m a -> m [a]
pBlock inner = braces $ newlines *> sepEndBy inner (nlsep ";")

pLoopStmt :: MonadParser m => m AstLoopStmt
pLoopStmt = choice
    [ symbol "continue" $> AstLoopStmtContinue AstContinue
    , symbol "break" $> AstLoopStmtBreak AstBreak
    , AstLoopStmtStmt <$> pStmt
    ]

pStmt :: MonadParser m => m AstStmt
pStmt = choice
    [ AstStmtVar <$> pVarDecl
    , AstStmtIf <$> pIf
    , AstStmtFor <$> pFor
    , AstStmtSwitch <$> pSwitch
    , AstStmtReturn <$> pReturn
    , AstStmtCall <$> try pCall
    , AstStmtOp <$> pOp
    ]

pLit :: MonadParser m => m AstLit
pLit = choice
    [ AstLitInt <$> fmap AstInt (try int)
    , AstLitFloat <$> fmap AstFloat float
    , AstLitStruct <$> pStructLit
    , AstLitEnum <$> pEnumLit
    ]

pStructLit :: MonadParser m => m AstStructLit
pStructLit = AstStructLit . M.fromList <$>
    pBlock ((,) <$> identifier <*> (symbol ":" *> pExpr))

pEnumLit :: MonadParser m => m AstEnumLit
pEnumLit = AstEnumLit <$> identifier <*> pExpr

pExprNoOp :: MonadParser m => m AstExpr
pExprNoOp = choice
    [ AstExprCall <$> try pCall
    , AstExprVar <$> pVar
    , AstExprLit <$> pLit
    ]

pExpr :: MonadParser m => m AstExpr
pExpr = choice
    [ AstExprOp <$> try pOp
    , pExprNoOp
    ]

pType :: MonadParser m => m AstType
pType = choice
    [ AstTypeStruct <$> pStruct
    , AstTypeEnum <$> pEnum
    , AstTypeNamed <$> fmap AstNamedType identifier
    ]

pFn :: MonadParser m => m AstFn
pFn = symbol "fn" *> (AstFn
        <$> identifier
        <*> pArgs
        <*> optional (symbol "->" *> identifier)
        <*> pBlock pStmt)
    where
        pArgs = parens $ newlines *> (M.fromList <$> sepEndBy
            ((,) <$> identifier <*> identifier) (nlsep ","))

pFixity :: MonadParser m => m AstFixity
pFixity = symbol "infix" *> (AstFixity
    <$> (symbol "left" $> AssocLeft <|> symbol "right" $> AssocRight)
    <*> int
    <*> operator
    <*> identifier)
 
pTypeDecl :: MonadParser m => m AstTypeDecl
pTypeDecl = symbol "type" *> (AstTypeDecl
    <$> identifier
    <*> pType)

pStruct :: MonadParser m => m AstStruct
pStruct = symbol "struct" *> (fmap AstStruct $ M.fromList <$> pBlock inner)
    where inner = (,) <$> identifier <*> pType

pEnum :: MonadParser m => m AstEnum
pEnum = symbol "enum" *> (fmap AstEnum $ M.fromList <$> pBlock inner)
    where inner = (,) <$> identifier <*> pType

pVarDecl :: MonadParser m => m AstVarDecl
pVarDecl = symbol "var" *> (AstVarDecl
    <$> identifier
    <*> identifier
    <*> (symbol "=" *> pExpr))

pIf :: MonadParser m => m AstIf
pIf = symbol "if" *> (AstIf
    <$> pExpr
    <*> pBlock pStmt
    <*> optional (symbol "else" *> pBlock pStmt))

pFor :: MonadParser m => m AstFor
pFor = symbol "for" *> symbol "(" *> (AstFor
        <$> optional pStmt
        <*> (semi *> optional pExpr)
        <*> (semi *> optional pStmt)
        <*> (symbol ")" *> pBlock pLoopStmt))
    where semi = symbol ";"

pSwitch :: MonadParser m => m AstSwitch
pSwitch = symbol "switch" *> (AstSwitch
        <$> pExpr
        <*> pBlock pSwitchInner)
    where
        pSwitchInner = choice [ pCase, pDefault ]
        pCase = symbol "case" *> (AstCase <$> (Just <$> pExpr) <*> pBlock pStmt)
        pDefault = symbol "default" *> (AstCase Nothing <$> pBlock pStmt)

pReturn :: MonadParser m => m AstReturn
pReturn = symbol "return" *> (AstReturn <$> pExpr)

-- | We don't handle operator precedence here. We do it in a post-processing step.
-- | We parse as if all operators are the same precedence and right associative.
pOp :: MonadParser m => m AstOp
pOp = AstOp <$> pExprNoOp <*> operator <*> pExpr

pVar :: MonadParser m => m AstVar
pVar = AstVar <$> sepBy1 identifier (chunk ".")

pCall :: MonadParser m => m AstCall
pCall = AstCall <$> identifier <*> parens (sepEndBy pExpr $ symbol ",")
