{-# LANGUAGE DeriveDataTypeable, NamedFieldPuns #-}

module Ast where

import Data.Map.Strict (Map)
import Data.Text (Text)
import Data.Data (Data)
import Data.Int (Int64)

data AstTop
    = AstTopFn AstFn
    | AstTopFixity AstFixity
    | AstTopType AstTypeDecl
    | AstTopVar AstVarDecl
    deriving (Show, Data)

data AstType
    = AstTypeStruct AstStruct
    | AstTypeEnum AstEnum
    | AstTypeNamed AstNamedType
    deriving (Show, Data)

data AstLoopStmt
    = AstLoopStmtBreak AstBreak
    | AstLoopStmtContinue AstContinue
    | AstLoopStmtStmt AstStmt
    deriving (Show, Data)

data AstStmt
    = AstStmtVar AstVarDecl
    | AstStmtIf AstIf
    | AstStmtFor AstFor
    | AstStmtSwitch AstSwitch
    | AstStmtReturn AstReturn
    | AstStmtOp AstOp
    | AstStmtCall AstCall
    deriving (Show, Data)

data AstExpr
    = AstExprOp AstOp
    | AstExprVar AstVar
    | AstExprCall AstCall
    | AstExprLit AstLit
    deriving (Show, Data)

data AstLit
    = AstLitStruct AstStructLit
    | AstLitEnum AstEnumLit
    | AstLitInt AstInt
    | AstLitFloat AstFloat
    deriving (Show, Data)

data Assoc = AssocLeft | AssocRight deriving (Show, Data)

data AstFixity = AstFixity
    { astFixityAssoc :: Assoc
    , astFixityPrec :: Int64
    , astFixityOp :: Text
    , astFixityFn :: Text
    } deriving (Show, Data)

data AstFn = AstFn
    { astFnName :: Text
    , astFnArgs :: Map Text Text
    , astFnReturns :: Maybe Text
    , astFnBody :: [AstStmt]
    } deriving (Show, Data)

data AstTypeDecl = AstTypeDecl
    { astTypeDeclName :: Text
    , astTypeDeclType :: AstType
    } deriving (Show, Data)

data AstStruct = AstStruct
    { astStructBody :: Map Text AstType
    } deriving (Show, Data)

data AstEnum = AstEnum
    { astEnumBody :: Map Text AstType
    } deriving (Show, Data)

data AstNamedType = AstNamedType
    { astNamedTypeName :: Text
    } deriving (Show, Data)

data AstVarDecl = AstVarDecl
    { astVarDeclName :: Text
    , astVarDeclType' :: Text
    , astVarDeclValue :: AstExpr
    } deriving (Show, Data)

data AstIf = AstIf
    { astIfCond :: AstExpr
    , astIfThen' :: [AstStmt]
    , astIfElse' :: Maybe [AstStmt]
    } deriving (Show, Data)

data AstFor = AstFor
    { astForInit :: Maybe AstStmt
    , astForCheck :: Maybe AstExpr
    , astForUpdate :: Maybe AstStmt
    , astForBody :: [AstLoopStmt]
    } deriving (Show, Data)

data AstSwitch = AstSwitch
    { astSwitchValue :: AstExpr
    , astSwitchBody :: [AstCase]
    } deriving (Show, Data)

data AstCase = AstCase
    { astCaseTest :: Maybe AstExpr
    , astCaseBody :: [AstStmt]
    } deriving (Show, Data)

data AstReturn = AstReturn
    { astReturnValue :: AstExpr
    } deriving (Show, Data)

data AstBreak = AstBreak deriving (Show, Data)

data AstContinue = AstContinue deriving (Show, Data)

data AstOp = AstOp
    { astOpLhs :: AstExpr
    , astOpOp :: Text
    , astOpRhs :: AstExpr
    } deriving (Show, Data)

data AstVar = AstVar
    { astVarRef :: [Text]
    } deriving (Show, Data)

data AstCall = AstCall
    { astCallName :: Text
    , astCallArgs :: [AstExpr]
    } deriving (Show, Data)

data AstInt = AstInt
    { astIntValue :: Int64
    } deriving (Show, Data)

data AstFloat = AstFloat
    { astFloatValue :: Double
    } deriving (Show, Data)

data AstStructLit = AstStructLit
    { astStructLitBody :: Map Text AstExpr
    } deriving (Show, Data)

data AstEnumLit = AstEnumLit
    { astEnumLitName :: Text
    , astEnumLitValue :: AstExpr
    } deriving (Show, Data)

data AstSymbols = AstSymbols
    { astOperators :: Map Text AstFixity
    , astFunctions :: Map Text AstFn
    , astVariables :: Map Text AstVarDecl
    , astTypes :: Map Text AstType
    } deriving (Show, Data)

emptyAstSymbols = AstSymbols mempty mempty mempty mempty
