{-# LANGUAGE DeriveDataTypeable #-}

module Core where

import Data.Text (Text)
import qualified Data.Text as T

import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M

import Data.Generics
