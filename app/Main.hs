{-# LANGUAGE LambdaCase #-}
module Main where

import Data.Text.IO as T

import Parser

main :: IO ()
main = parseFile "test.mlang" >>= \case
	Left err -> T.putStr err
	Right ast -> print ast
